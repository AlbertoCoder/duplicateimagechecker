import os
from PIL import Image
from PIL import ImageChops
import numpy as np

fileNames = os.listdir()
indexMax = len(fileNames)

i = 0
j = i+1

#Compare two images and checks if they are the same
def compareImages(image1, image2):
	#Shape checks if the images have the same resolution
	#If they don't they can't be duplicate images
	if image1.shape != image2.shape:
		return False
	difference = image2 - image1
	if np.all(difference == 0):
		return True
	return False

#Deletes the image in the folder with the name passed in input
def deleteImage(image , index):
	fileNames.pop(index)
	os.remove(image)
	print("Removed File: ", image)
	

#change ".jpg" to whatever picture format you want like ".png"
#You have to change to both sides. This program compares only images of the same type.

while i < indexMax:
	if fileNames[i].endswith(".jpg"):
		first = np.array(Image.open(fileNames[i]))
		while j < indexMax:
			if fileNames[j].endswith(".jpg"):
				second = np.array(Image.open(fileNames[j]))
				if compareImages(first, second):
					print("Equal files: ", fileNames[i], fileNames[j])
					deleteImage(fileNames[j], j)
					#This updates the two variables for the ranges
					fileNames = os.listdir()
					indexMax = len(fileNames)
			j += 1
	#Reset indexes to restart the loop
	i +=1
	j = i+1
	print("Pictures checked: ", i)
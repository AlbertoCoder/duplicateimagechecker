# DuplicateImageChecker

This simple python script compares all the pictures of a folder and checks for duplicates. If it does find one, it deletes the duplicate.
Requires:
- Numpy
- Pillow